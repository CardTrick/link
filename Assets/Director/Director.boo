import UnityEngine

class Director (MonoBehaviour): 

	public playerMaterials as (Material)
	public childMaterials as (Material)
	public gameOver = false
	public winner as Player = null
	
	
	static main as Director:
		get: return GameObject.FindWithTag("GameController").GetComponent(Director)
		
	
	def GetMaterialForPlayer(playerNum as int):
		return playerMaterials[playerNum-1]
	
	def GetMaterialForChildOf(playerNum as int):
		return childMaterials[playerNum-1]
	
	def EndGame():
		gameOver = true
		player1 = GameObject.Find("Player 1").GetComponent(Player)
		player2 = GameObject.Find("Player 2").GetComponent(Player)
		if player1.score != player2.score:
			winner = (player1 if player1.score > player2.score else player2)
			loser = (player1 if player1.score < player2.score else player2)
			self.ExplodeLoser(loser)
	
	def ExplodeLoser(loser as Player):
		nodes = List[of Transform](loser.allNodes)
		loser.GetComponent(PlayerNode).UnparentTree()
		for child in nodes:
			child.rigidbody.AddExplosionForce(100.0, loser.transform.position, 100.0)
		loser.dead = true
	
	def Update():		
		if Input.GetKeyDown(KeyCode.Escape):
			Application.LoadLevel("Splash Scene")
		if Input.GetKeyDown(KeyCode.F8):
			Time.timeScale = 5 - Time.timeScale
		if Input.GetKeyDown(KeyCode.F5):
			Application.LoadLevel(Application.loadedLevel)
		if Input.GetKeyDown(KeyCode.F2):
			self.SwitchCamera()
	
	def SwitchCamera():
		cam1 = GameObject.FindWithTag("MainCamera")
		cam2 = GameObject.Find("Camera2")
		if cam1.camera.enabled:
			cam1.camera.enabled = false
			cam2.camera.enabled = true
		else:
			cam1.camera.enabled = true
			cam2.camera.enabled = false