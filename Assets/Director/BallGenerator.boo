import UnityEngine

class BallGenerator (MonoBehaviour): 

	public ballPrefab as GameObject
	public bombPrefab as GameObject
	public superBombPrefab as GameObject
	public greenNodePrefab as GameObject
	

	capital = 20
	ballTime = 3.0
	timeLeft = 3.0
	
	
	def Start():
		for i in range(capital):
			self.GenerateBall()

	def Update ():
		if Director.main.gameOver:
			return
		timeLeft -= Time.deltaTime
		if timeLeft <= 0:
			self.GenerateBall()
			self.ballTime *= 0.93
			if self.ballTime < 0.8:
				self.ballTime = 0.8
			timeLeft = ballTime
	
	def GenerateBall():
		aspect = 16.0/9.0
		maxRadius = 16.0
		randR = Random.value * maxRadius
		randTheta = Random.value * Mathf.PI * 2
		pos = Vector3(randR * Mathf.Cos(randTheta) * aspect, randR * Mathf.Sin(randTheta), 0)
		thing = (ballPrefab if Random.value > 0.15 else bombPrefab)
		if Random.value < 0.01:
			thing = greenNodePrefab
		if Random.value < 0.005:
			thing = superBombPrefab
		Instantiate(thing, pos, Quaternion.identity)
