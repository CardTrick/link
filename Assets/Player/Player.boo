import UnityEngine

class Player (MonoBehaviour): 

	public playerNumber = 1
	public speed = 50.0
	
	public allNodes = List[of Transform]()
	bombs = List[of Bomb]()
	
	public dead = false
	
	
	score as int:
		get: return _score
		
	_score = 0
	
	
	def Start():
		renderer.material = Director.main.GetMaterialForPlayer(playerNumber)
		node = self.GetComponent(PlayerNode)
		node.player = self
		node.isFree = false

	def FixedUpdate():
		if dead:
			return
		xmove = Input.GetAxis("Player $(playerNumber) X")
		ymove = Input.GetAxis("Player $(playerNumber) Y")
		input = Vector3(xmove, ymove, 0)
		if input.magnitude < 0.2:
			input = Vector3.zero
		if input.magnitude > 1.0:
			input = input.normalized
		rigidbody.AddForce(input * speed)
	
	def Update():
		if Input.GetButtonDown("Player $(playerNumber) Fire"):
			if len(bombs) > 0:
				bomb = bombs[0]
				bomb.Trigger()
	
	def RecalcScore():
		s = 0
		for node in allNodes:
			s += node.GetComponent(PlayerNode).score
		return s
	
	def AddNode(child as PlayerNode):
		allNodes.Add(child.transform)
		if child.bomb:
			bombs.Add(child.bomb)
		_score = self.RecalcScore()
	
	def RemoveNode(child as PlayerNode):
		allNodes.Remove(child.transform)
		if child.bomb and child.bomb in bombs:
			bombs.Remove(child.bomb)
		_score = self.RecalcScore()