import UnityEngine

class MusicBox (MonoBehaviour): 

	public stickAround as bool = true
	
	sceneChangedSinceBirth = false
	parentSceneName as string
	
	
	def Start():
		self.parentSceneName = Application.loadedLevelName
		if stickAround:
			DontDestroyOnLoad(self.gameObject)
	
	def OnLevelWasLoaded():
		if sceneChangedSinceBirth and Application.loadedLevelName == parentSceneName:
			/* This is the scene that the music box was created in, but it has since been reloaded.
				To avoid having two music boxes, it will now destroy itself. */
			Destroy(self.gameObject)
		self.sceneChangedSinceBirth = true