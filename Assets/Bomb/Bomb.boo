import UnityEngine
import System.Collections

class Bomb (MonoBehaviour): 

	public isSuper = false
	public explosionPrefab as GameObject
	public glowMaterial as Material
	public normalMaterial as Material

	lifetime = 2.0
	public explosionRadius = 5.0
	node as PlayerNode
	dangerous = false
	
	
	def Start():
		node = self.GetComponent(PlayerNode)

	def Trigger():
		dangerous = true
		Destroy(node.attractor.gameObject)
		node.Unparent()
		self.gameObject.layer = 9 // Walls only
		StartCoroutine(self.Explode())
		StartCoroutine(self.Pulsanimate())
	
	def Explode() as IEnumerator:
		yield WaitForSeconds(lifetime)
		hits = Physics.SphereCastAll(transform.position, explosionRadius, Vector3.up, 1.0)
		if isSuper:
			for hit in hits:
				bomb = hit.transform.GetComponent(Bomb)
				if bomb and not bomb.dangerous:
					bomb.Trigger()
		for hit in hits:
			hitnode = hit.transform.GetComponent(PlayerNode)
			if not hitnode:
				continue
			hitnode.UnparentTree()
			hitnode.rigidbody.AddExplosionForce(20.0, transform.position, explosionRadius)
		explosion = Instantiate(explosionPrefab, transform.position, Quaternion.identity)
		explosion.GetComponent(ParticleSystem).startSpeed *= (explosionRadius/4.0)
		Destroy(explosion, 2.0)
		Destroy(self.gameObject)
	
	def Pulsanimate() as IEnumerator:
		pulses = 0
		while true:
			pulses += 1
			StartCoroutine(self.Pulse())
			t = (lifetime/8.0 if pulses > 2 else lifetime/4.0)
			yield WaitForSeconds(t)
	
	def Pulse() as IEnumerator:
		self.audio.Play()
		renderer.material = glowMaterial
		yield WaitForSeconds(0.1)
		renderer.material = normalMaterial