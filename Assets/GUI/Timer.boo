import UnityEngine

class Timer (MonoBehaviour): 

	ticking = true
	timeRemaining = 60.0
	text as TextMesh
	
	
	def Start():
		text = transform.Find("Text").GetComponent(TextMesh)
	
	def Update():
		if ticking:
			timeRemaining -= Time.deltaTime
			if timeRemaining <= 0:
				timeRemaining = 0.0
				ticking = false
				Director.main.EndGame()
			text.text = timeRemaining.ToString("0.00")
