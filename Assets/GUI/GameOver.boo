import UnityEngine


class GameOver (MonoBehaviour): 

	text as TextMesh
	done = false
	winColour as string
	
	
	def Start():
		text = transform.Find("Text").GetComponent(TextMesh)
	
	def Update():
		if not done:
			if Director.main.gameOver:
				if Director.main.winner == null:
					text.text = "<b>Draw.</b>"
				else:
					winnum = Director.main.winner.playerNumber
					winColour = ("#00ffff" if winnum == 1 else "#ffff00")
					text.text = "<color=$(winColour)><b>Player $(winnum) wins!</b></color>"
				text.gameObject.SetActive(true)
				done = true