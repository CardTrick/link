import UnityEngine

class Scoreboard (MonoBehaviour): 

	public player as Player
	public color as string
	text as TextMesh
	
	lastScore = -1
	
	def Start():
		text = transform.Find("Text").GetComponent(TextMesh)
	
	def Update():
		if Director.main.gameOver:
			return
		s = player.score
		if s != lastScore:
			lastScore = s
			text.text = "<color=$(color)>$(player.gameObject.name): $(s)</color>"