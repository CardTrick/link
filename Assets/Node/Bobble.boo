import UnityEngine

class Bobble (MonoBehaviour):
	
	public bobHeight = 0.2
	public bobSpeed = 1.0

	bobbleAmount = 0.0
	previousBobble = 0.0
	timeSoFar = 0.0
	node as PlayerNode 
	
	
	def Start():
		self.node = self.GetComponent(PlayerNode)
		timeSoFar += Random.value * Mathf.PI * 2
	
	def Update():
		if node.parent:
			return

		timeSoFar += Time.deltaTime
		previousBobble = bobbleAmount
		bobbleAmount = Mathf.Sin(timeSoFar * bobSpeed)
		
		difference = bobbleAmount - previousBobble
		transform.position += Vector3.up * difference * bobHeight