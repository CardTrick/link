import UnityEngine

class PlayerNode (MonoBehaviour): 

	public score = 1
	public retainsColour = false

	public player as Player = null
	public parent as Transform = null
	public isFree = true
	
	public childNodes = List[of Transform]()
	public line as Transform
	public attractor as Transform
	public bomb as Bomb
	public cooldown = 0.0
	
	available as bool:
		get: return (bomb == null and not isFree)
	cooledDown as bool:
		get: return cooldown <= 0.0
	
	
	def Start():
		line = transform.Find("Line")
		attractor = transform.Find("AttractZone")
		bomb = self.GetComponent(Bomb)

	def ParentTo(node as PlayerNode):
		self.isFree = false
		self.parent = node.transform
		self.player = node.player
		mat = Director.main.GetMaterialForChildOf(player.playerNumber)
		if not retainsColour:
			renderer.material = mat
		self.player.AddNode(self)
		node.childNodes.Add(transform)
		if line != null:
			line.gameObject.SetActive(true)
			line.GetComponent(LineRenderer).material = mat
		if attractor != null:
			attractor.collider.enabled = false
		self.CreateJoint()
	
	def Unparent():
		if not self.parent:
			return
		self.isFree = true
		self.parent.GetComponent(PlayerNode).childNodes.Remove(transform)
		self.player.RemoveNode(self)
		self.player = null
		self.parent = null
		if line != null:
			line.gameObject.SetActive(false)
		if attractor != null:
			attractor.collider.enabled = true
		self.DestroyJoint()
		self.cooldown = 1.2
	
	def UnparentTree():
		if self.parent != null:
			self.Unparent()
		for child as Transform in List(self.childNodes):
			child.GetComponent(PlayerNode).UnparentTree()
	
	def DestroyJoint():
		joint = self.GetComponent(SpringJoint)
		if joint:
			Destroy(joint)
	
	def CreateJoint():
		joint = gameObject.AddComponent(SpringJoint)
		joint.connectedBody = self.parent.rigidbody
		joint.spring = 1.2
		joint.damper = 0.0
		joint.minDistance = 0.2
		joint.maxDistance = 0.4

	def Update():
		if cooldown > 0:
			cooldown -= Time.deltaTime
		if line != null and line.gameObject.activeInHierarchy:
			lr = line.GetComponent(LineRenderer)
			lr.SetPosition(0, transform.position)
			lr.SetPosition(1, parent.position)