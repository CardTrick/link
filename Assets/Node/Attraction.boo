import UnityEngine

class Attraction (MonoBehaviour):
	
	public collectSounds as (AudioClip)
	public sleepFrames = 10
	
	def Update():
		if sleepFrames > 0:
			sleepFrames -= 1

	def OnTriggerEnter(other as Collider):
		if sleepFrames > 0:
			return
		node = other.GetComponent(PlayerNode)
		if node != null:
			thisNode = transform.parent.GetComponent(PlayerNode)
			if thisNode != null and thisNode.isFree and node.available and thisNode.cooledDown:
				i = Random.Range(0, len(collectSounds))
				self.audio.clip = collectSounds[i]
				self.audio.Play()
				thisNode.ParentTo(node)